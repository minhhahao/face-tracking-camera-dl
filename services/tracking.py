import cv2
import dlib
import numpy as np
import tensorflow as tf

from imutils.face_utils import FaceAligner
from imutils.face_utils import rect_to_bb
from time import gmtime, strftime
from libs import inception_resnet_v1


class TrackingCamera(object):
    def __init__(self, checkpoint_mode_path, dlib_detect_model_path):
        self.checkpoint_mode_path = checkpoint_mode_path
        self.font_scale = 1
        self.thickness = 2
        self.font = cv2.FONT_HERSHEY_SIMPLEX
        self.dlib_detect_model_path = dlib_detect_model_path

        # for face detection
        self.detector = dlib.get_frontal_face_detector()
        self.predictor = dlib.shape_predictor(self.dlib_detect_model_path)
        self.fa = FaceAligner(self.predictor, desiredFaceWidth=160)

    def restore_model(self):
        sess = tf.Session()
        images_pl = tf.placeholder(tf.float32, shape=[None, 160, 160, 3], name='input_image')
        images_norm = tf.map_fn(lambda frame: tf.image.per_image_standardization(frame), images_pl)
        train_mode = tf.placeholder(tf.bool)
        age_logits, gender_logits, _ = inception_resnet_v1.inference(images_norm, keep_probability=0.8,
                                                                     phase_train=train_mode,
                                                                     weight_decay=1e-5)
        gender = tf.argmax(tf.nn.softmax(gender_logits), 1)
        age_ = tf.cast(tf.constant([i for i in range(0, 101)]), tf.float32)
        age = tf.reduce_sum(tf.multiply(tf.nn.softmax(age_logits), age_), axis=1)
        init_op = tf.group(tf.global_variables_initializer(),
                           tf.local_variables_initializer())
        sess.run(init_op)
        saver = tf.train.Saver()
        ckpt = tf.train.get_checkpoint_state(self.checkpoint_mode_path)
        if ckpt and ckpt.model_checkpoint_path:
            saver.restore(sess, ckpt.model_checkpoint_path)
            print("Restore model!")
        else:
            pass
        return sess, age, gender, train_mode, images_pl

    def draw_label(self, image, point, label):
        cv2.putText(image, label, point, self.font, self.font_scale, (0, 0, 255), self.thickness, cv2.LINE_AA)

    def video_handle(self, sess, age, gender, train_mode, images_pl, upload_media=None, media_path=None):
        # load model and weights
        img_size = 160

        if upload_media == "video":
            cap = cv2.VideoCapture(media_path)
            cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
            cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
        else:
            cap = cv2.VideoCapture(0)
            cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
            cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)

        while True:
            ret, img = cap.read()

            if not ret:
                print("error: failed to capture image")
                break


            input_img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            img_h, img_w, _ = np.shape(input_img)

            # detect faces using dlib detector
            detected = self.detector(input_img, 1)
            faces = np.empty((len(detected), img_size, img_size, 3))

            for i, d in enumerate(detected):
                x1, y1, x2, y2, w, h = d.left(), d.top(), d.right() + 1, d.bottom() + 1, d.width(), d.height()
                cv2.rectangle(img, (x1, y1), (x2, y2), (0, 0, 255), 2)
                faces[i, :, :, :] = self.fa.align(input_img, gray, detected[i])

            if len(detected) > 0:
                # predict ages of the detected faces
                ages, _ = sess.run([age, gender], feed_dict={images_pl: faces, train_mode: False})

            # draw results
            for i, d in enumerate(detected):
                label = "{}".format(int(ages[i]))
                self.draw_label(img, (d.left(), d.top() - 15), label)

            img_encode = cv2.imencode('.jpg', img)[1]
            string_data = img_encode.tostring()

            yield (b'--frame\r\n'
                   b'Content-Type: text/plain\r\n\r\n' + string_data + b'\r\n')

        del cap

    def image_handle(self, sess, age, gender, train_mode, images_pl, media_path):
        image = cv2.imread(media_path, cv2.IMREAD_COLOR)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        detected = self.detector(gray, 1)
        rect_nums = len(detected)

        aligned_images = []
        if rect_nums > 0:
            for i in range(rect_nums):
                aligned_image = self.fa.align(image, gray, detected[i])
                aligned_images.append(aligned_image)
                (x, y, w, h) = rect_to_bb(detected[i])
                image = cv2.rectangle(image, (x, y), (x + w, y + h), color=(0, 0, 255), thickness=2)

            aligned_images = np.array(aligned_images)
            ages, _ = sess.run([age, gender], feed_dict={images_pl: aligned_images, train_mode: False})
            # draw results
            for i, d in enumerate(detected):
                label = "{}".format(int(ages[i]))
                self.draw_label(image, (d.left(), d.top() - 15), label)

            time_now = strftime("%Y%m%d_%H%M%S", gmtime())
            image_path = "static/images/{}.jpg".format(time_now)
            file_name = "{}.jpg".format(time_now)
            cv2.imwrite(image_path, image)

            return image_path, file_name
        else:
            return
