import ConfigParser
import os
from flask import Flask, render_template, Response, request, jsonify
from werkzeug.utils import secure_filename

from services.tracking import TrackingCamera

CONFIG_FILE = "env_dev.ini"
conf = ConfigParser.SafeConfigParser()
conf.read(CONFIG_FILE)

UPLOAD_FOLDER = "uploads"

application = Flask(__name__)
application.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

track_camera = TrackingCamera(
        checkpoint_mode_path=conf.get("nmtq-path", "checkpoint_mode_path"),
        dlib_detect_model_path=conf.get("nmtq-path", "dlib_detect_model")
    )
sess, age, gender, train_mode, images_pl = track_camera.restore_model()


@application.route('/index')
@application.route('/')
def index():
    return render_template('home.html')


@application.route('/stream')
def stream():
    return render_template('stream.html')


@application.route('/video_stream')
def video_stream():
    estimate = track_camera.video_handle(sess, age, gender, train_mode, images_pl)
    return Response(estimate, mimetype='multipart/x-mixed-replace; boundary=frame')


@application.route('/uploader_image', methods=['GET', 'POST'])
def uploader_image():
    if request.method == 'POST':
        file = request.files['file']
        filename = secure_filename(file.filename)
        storage_file = os.path.join(application.config['UPLOAD_FOLDER'], filename)
        file.save(storage_file)

        try:
            image_path, file_name = track_camera.image_handle(sess, age, gender, train_mode, images_pl, media_path=storage_file)

            return jsonify({
                'status': 200,
                'image_path': image_path,
                'file_name': file_name
            })
        except TypeError:
            return jsonify({
                'status': 404
            })
    else:
        return render_template('image_detect.html')


@application.route('/uploader_video', methods=['GET', 'POST'])
def uploader_video():
    if request.method == 'POST':
        file = request.files['file']
        filename = secure_filename(file.filename)
        storage_file = os.path.join(application.config['UPLOAD_FOLDER'], filename)
        file.save(storage_file)

        return jsonify({
                'status': 200,
                'file_name': filename
            })

    else:
        filename = request.args.get("file")
        storage_file = os.path.join(application.config['UPLOAD_FOLDER'], filename)
        estimate = track_camera.video_handle(sess, age, gender, train_mode, images_pl,
                                             upload_media="video", media_path=storage_file)
        return Response(estimate, mimetype='multipart/x-mixed-replace; boundary=frame')


if __name__ == '__main__':
    application.run(debug=True, threaded=True)
