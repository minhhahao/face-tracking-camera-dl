(function ($) {
    'use strict';
    var startUploadImage = function (file) {
        var data = new FormData();
        data.append('file', file);
        $.ajax({
            url: "/uploader_image",
            type: "POST",
            data: data,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function () {

            },
            error: function (error) {
            },
            success: function (result) {
                $(".loader").css("display", "none");
                if (result["status"] === 200) {
                    $(".upload-drop-zone").css("display", "none");
                    $("#img-response").attr("src", result["image_path"]);
                    $(".image-result").css("margin-bottom", "15px");
                    $("#download-image").attr("href", result["image_path"]);
                    $("#download-image").attr("download", result["file_name"]);
                }
                if (result["status"] === 404) {
                    $(".upload-media-title").css("display", "block");
                    alert("Unable to detect face. Please try another image or video ")
                }

            }
        });
    };

    var startUploadVideo = function (file) {
        var data = new FormData();
        data.append('file', file);
        $.ajax({
            url: "/uploader_video",
            type: "POST",
            data: data,
            cache: false,
            processData: false,
            contentType: false,
            error: function (error) {
            },
            success: function (result) {
                $(".loader").css("display", "none");

                if (result["status"] === 200) {
                    var fullVideoUrl = "/uploader_video?file=" + result["file_name"];
                    $("#img-response").attr("src", fullVideoUrl);
                }
            }
        });
    };

    var getExtension = function (filename) {
        var parts = filename.split('.');
        return parts[parts.length - 1];
    };

    var isImage = function (filename) {
        var ext = getExtension(filename);
        switch (ext.toLowerCase()) {
            case 'jpg':
            case 'bmp':
            case 'png':
                return true;
        }
        return false;
    };

    var isVideo = function (filename) {
        var ext = getExtension(filename);
        switch (ext.toLowerCase()) {
            case 'm4v':
            case 'avi':
            case 'mpg':
            case 'mp4':
                return true;
        }
        return false;
    };

    $("#js-upload-files").change(function (e) {
        e.preventDefault();
        var uploadFiles = document.getElementById('js-upload-files').files;

        if (isImage(uploadFiles[0].name)) {
            $(".loader").css("display", "block");
            $(".upload-media-title").css("display", "none");
            startUploadImage(uploadFiles[0]);
        } else if (isVideo(uploadFiles[0].name)) {
            $(".loader").css("display", "block");
            $(".upload-media-title").css("display", "none");
            startUploadVideo(uploadFiles[0]);
        } else {
            console.log("Fun Fact!");
            alert("Please upload file having extensions .jpeg/.jpg/.png/.mp4/.avi only.")
        }
    });
})(jQuery);