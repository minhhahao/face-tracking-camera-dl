#!/usr/bin/env bash

python test.py --images "./data/test" --model_path "./models/checkpoint_model/" --batch_size 128 --cuda --choose_best